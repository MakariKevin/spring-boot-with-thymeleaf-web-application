package com.studies.sms;

import com.studies.sms.entity.Student;
import com.studies.sms.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentManagementSystemApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(StudentManagementSystemApplication.class, args);
	}

	@Autowired
	private StudentRepository studentRepository;

	@Override
	public void run(String... args) throws Exception {
		/*Student student1 = new Student("Kevin", "Makari", "kevimakari@gmail.com");
		studentRepository.save(student1);

		Student student2 = new Student("John", "Doe", "jdoe@gmail.com");
		studentRepository.save(student2);

		Student student3 = new Student("Peter", "Waweru", "pwaweru@gmail.com");
		studentRepository.save(student3);

		Student student4 = new Student("Alex", "Jude", "ajude@gmail.com");
		studentRepository.save(student4);*/
	}
}
