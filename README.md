Spring Boot with Thymeleaf Web Application - Student Management System

This is a simple Student Management System web application using Spring Boot, Spring MVC, Thymeleaf, Spring Data JPA, and MySQL database.

<ins>**Tools and technologies used:**</ins>
- Java 16
- Spring Boot
- Spring MVC
- Spring Data JPA ( Hibernate)
- MySQL
- Thymeleaf
- IntelliJ IDEA

<ins>**Output**</ins>

![Screenshot 2022-10-19 at 11.01.30.png](./Screenshot 2022-10-19 at 11.01.30.png)

![Screenshot 2022-10-19 at 11.55.53.png](./Screenshot 2022-10-19 at 11.55.53.png)

![Screenshot 2022-10-19 at 11.56.15.png](./Screenshot 2022-10-19 at 11.56.15.png)


